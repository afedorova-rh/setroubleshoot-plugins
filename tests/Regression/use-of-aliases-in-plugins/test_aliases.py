#!/usr/bin/python3

# lists all types not defined in the policy as "type_t not found"
# and all aliases as "alias_t is an alias of type_t"
# all issues are prefixed with a list of offending plugins
# returns 1 if an issue was found

import subprocess
import sepolicy
import sys
import re
from collections import defaultdict

plugin_path = "/usr/share/setroubleshoot/plugins"
error_code = 0

if len(sys.argv) > 1:
	plugin_path = sys.argv[1]

try:
	# search all plugin files in given location for the following pattern
	# <plugin path>:<delimiter><type name>_t<delimiter>
	g = subprocess.check_output('grep -I [^A-Za-z_][A-Za-z][A-Za-z_]*_t[^A-Za-z_] -o {}/*.py'.format(plugin_path),
				     universal_newlines=True, shell=True)
	lines = g.split('\n')
except:
	exit(1)
# matches 2 groups: file name and type name
# <path to plugins>(<plugin file name>):<delimiter>(<type name>_t)<delimiter>
reg = re.compile('.*/(.+):[^A-Za-z_]([A-Za-z_]*_t)[^A-Za-z_]')
# generate a dictionary of of all type names used in setroubleshoot plugins
# where types are keys and lists of files where each type appeared are data
found = defaultdict(set)

for l in lines:
	m = reg.match(l)

	if m is None:
		continue

	try:
		t = m.group(2)
		if "_TYPE_" in t:
			continue
		found[t].add(m.group(1))
	except:
		# failed to match
		continue

for t in sorted(found.keys()):
	try:
		# try to find each type in system policy
		i = next(sepolicy.info(sepolicy.TYPE, t))['name']
		if t != i:
			# <plugin file names>: alias_t is an alias of type_t
			print("{}: {} is an alias of {}".format(", ".join(found[t]), t, i))
			error_code = 1
	except:
		# skip types defined in selinux-policy modules that are not shipped any more
		if t not in ["vbetool_t"]:
			# <plugin file names>: type_t not found
			print("{}: {} not found".format(", ".join(found[t]), t))
			error_code = 1

exit(error_code)
